__autor__ = "Бугайчук Олег"

matrix = [[1, 0, 8],
        [3, 4, 1],
        [0, 4, 2]]

#выполнение в одну строку
matrix_rotate = tuple(zip(*matrix[::-1]))

print("{} \n{} \n{}".format(matrix_rotate[0],matrix_rotate[1], matrix_rotate[2]))