class Personal:

    def __init__(self, name = "", surname = "", norm = "", hours = ""):
        self.__norm = norm
        self.__hours = hours
        self.__name = name
        self.__surname = surname


    def calcMoney(self):
        '''
        функция вычисляет зп сотрудника и возвращает сумму
        '''
                 
        normPerHour = int(self.__norm[2]) / int(self.__norm[4])
        cash = 0
        pererab = int(self.__hours[2]) - int(self.__norm[4])
        if pererab <= 0:
            cash = int(normPerHour) * int(self.__hours[2])
        else:
            cash = ((int(self.__hours[2]) - pererab) * normPerHour) + (pererab * (normPerHour * 2)) 
        return(int(cash))
        
    




    '''
    --------------------------------------------------Get, Set--------------------------------------------------------------
    '''
    def setHours(self, hours):
        '''
        добавить строку с отработанными часами из файла
        '''
        hours = hours.split(" ")
        self.__hours = [x for x in hours if x] 
    

    def setNorm(self, norm):
        '''
        добавить строку с нормой часов и зп из файла
        '''
        norm = norm.split(" ")
        self.__norm = [x for x in norm if x] 


    def setName(self, name):
        '''
        указать имя работника
        '''
        self.__name = name

    def setSurname(self, surname):
        '''
        указать фамилию работника
        '''
        self.__surname = surname
    

    def getName(self):
        '''
        возвращает имя сотрудника
        '''
        return self.__name
    
    def getSurname(self):
        '''
        возвращает фамилию сотрудника
        '''
        return self.__surname


    def getHours(self):
        '''
        возвращает количество отработанных часов
        '''
        return self.__hours


    def getNorm(self):
        '''
        возвращает норму
        '''
        return self.__norm