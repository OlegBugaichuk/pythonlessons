__autor__ = "Бугайчук Олег"

from Personal import Personal


PetrAlekseev = Personal("Петр", "Алексеев")
VasiliyIvanov = Personal("Василий", "Иванов")
MatveyBurin = Personal("Матвей", "Бурин")
VasilyiSidorov = Personal("Василий", "Сидоров")
PetrDurin = Personal("Петр", "Дурин")
AlbertGribov = Personal("Альберт", "Грибов")

with open("data\\workers", "r", encoding='UTF-8') as workers:
    for worker in workers:
        if worker.find(PetrAlekseev.getSurname()) > 0:
            PetrAlekseev.setNorm(worker)
        elif worker.find(VasiliyIvanov.getSurname()) > 0:
            VasiliyIvanov.setNorm(worker)
        elif worker.find(MatveyBurin.getSurname()) > 0:
            MatveyBurin.setNorm(worker)
        elif worker.find(VasilyiSidorov.getSurname()) > 0:
            VasilyiSidorov.setNorm(worker)
        elif worker.find(PetrDurin.getSurname()) > 0:
            PetrDurin.setNorm(worker)
        elif worker.find(AlbertGribov.getSurname()) > 0:
            AlbertGribov.setNorm(worker)

with open("data\\hours_of", "r", encoding='UTF-8') as hours:
    for hour in hours:
        if hour.find(PetrAlekseev.getSurname()) > 0:
            PetrAlekseev.setHours(hour)
        elif hour.find(VasiliyIvanov.getSurname()) > 0:
            VasiliyIvanov.setHours(hour)
        elif hour.find(MatveyBurin.getSurname()) > 0:
            MatveyBurin.setHours(hour)
        elif hour.find(VasilyiSidorov.getSurname()) > 0:
            VasilyiSidorov.setHours(hour)
        elif hour.find(PetrDurin.getSurname()) > 0:
            PetrDurin.setHours(hour)
        elif hour.find(AlbertGribov.getSurname()) > 0:
            AlbertGribov.setHours(hour)

print("Петр Алексеев --- {} руб.".format(PetrAlekseev.calcMoney()))
print("Василий Иванов --- {} руб.".format(VasiliyIvanov.calcMoney()))
print("Матвей Бурин --- {} руб.".format(MatveyBurin.calcMoney()))
print("Василий Сидоров --- {} руб.".format(VasilyiSidorov.calcMoney()))
print("Петр Дурин --- {} руб.".format(PetrDurin.calcMoney()))
print("Альберт Грибов --- {} руб.".format(AlbertGribov.calcMoney()))