__autor__ = "Бугайчук Олег"


import os
import sys
from shutil import copyfile
print('sys.argv = ', sys.argv)

def cp_dir():
    '''
    функция создает копию указанного файла
    '''
    if not dir_name:
        print("необходимо указать имя файла")
        return
    else:
               
        try:
            copyfile(os.path.join(os.getcwd(), dir_name), os.path.join(os.getcwd(), dir_name + "(copy)"), follow_symlinks=True)
        except FileNotFoundError:
            print("Файл не найден")
        except PermissionError:
            print("Нет доступа.")


def rm_file():
    '''
    функция удаляет указанный файл
    '''
    if not dir_name:
        print("необходимо указать имя файла")
        return
    else:
        try:
            os.remove(dir_name)
        except FileNotFoundError:
            print("Файл не найден")
        except PermissionError:
            print("Нет доступа.")


def cd_path():
    '''
    функция для смены рабочей директории
    '''
    if os.path.exists(os.path.join(os.getcwd(),dir_name)):
        os.chdir(dir_name)
        print(os.getcwd())
    elif os.path.exists(dir_name):
        os.chdir(dir_name)
        print(os.getcwd())
    else:
        print("Директория не найдена")

def ls_path():
    '''
    функция для вывода списка файлов и папок в текущей директрии
    '''
    list_dir = os.listdir(os.getcwd())
    for i in list_dir:
        print(i)



def print_help():
    print("help - получение справки")
    print("mkdir <dir_name> - создание директории")
    print("ping - тестовый ключ")
    print("cp <file_name> - копирование файла в текущей директории")
    print("rm <file_name> - удаление файла в текущей директории")
    print("cd <directory_path> - путь к папке")
    print("ls - вывести список файлов в текущей директории")


def make_dir():
    if not dir_name:
        print("Необходимо указать имя директории вторым параметром")
        return
    dir_path = os.path.join(os.getcwd(), dir_name)
    try:
        os.mkdir(dir_path)
        print('директория {} создана'.format(dir_name))
    except FileExistsError:
        print('директория {} уже существует'.format(dir_name))


def ping():
    print("pong")

do = {
    "help": print_help,
    "mkdir": make_dir,
    "ping": ping,
    "cp": cp_dir,
    "rm": rm_file,
    "cd": cd_path,
    "ls": ls_path
}

try:
    dir_name = sys.argv[2]
except IndexError:
    dir_name = None

try:
    key = sys.argv[1]
except IndexError:
    key = None


if key:
    if do.get(key):
        do[key]()
    else:
        print("Задан неверный ключ")
        print("Укажите ключ help для получения справки")
