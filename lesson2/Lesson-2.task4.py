
__author__ = 'Бугайчук Олег'

#функция принимает список и возвращает список без повторений
def listNoRepeat(newList):
    list_1 = []

    for i in newList:
        if i in list_1:
            continue
        else:
            list_1.append(i)
    
    return list_1

#принимает список и возвращает новый список с элементами которые не повторяются
def list2(newList):
    list_2 = []

    for i in newList:
        
        if newList.count(i) >= 2:
            continue
        else:
            list_2.append(i)

    return list_2    
    


#произвольный список
newList = [1, 2, 4, 8, 2, 8]

list_1 = listNoRepeat(newList)
list_2 = list2(newList)

print(list_1)
print(list_2)



