import math

__author__ = 'Бугайчук Олег'


#извлекает корень из элемента списка, в зависимости от условия и возвращает новый список 
def sqrtList(listNumbers):
    
    newlist = []
    for number in listNumbers:
        
        if number > 0:
            if (math.sqrt(number)%1) == 0 :
            
                newlist.append(int(math.sqrt(number)))
            else:
                continue
        else:
            continue

    return newlist



#произвольный список
listNumbers = [10, -9, 9, 25, 81]
#полученный список
newlist = sqrtList(listNumbers)

print(newlist)





