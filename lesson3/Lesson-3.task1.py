__author__ = 'Бугайчук Олег'


def fibonachi(n):
    '''
    Вычисляет n-ое число фибоначи
    '''
    if n > 1:
        return fibonachi(n-1) + fibonachi(n-2)
    return n

n = int(input("Введите номер начального числа фибоначи: "))
m = int(input("Теперь последнего: "))

while n <= m:
    
    fib_n = fibonachi(n)
    print("{} число: {}".format(n, fib_n))
    n  = n +1