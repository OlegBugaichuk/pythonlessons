__author__ = 'Бугайчук Олег'


def sort_to_max(origin_list):
    '''
    сортирует список методом пузырька
    '''
    n = 1 
    while n < len(origin_list):
        for i in range(len(origin_list)-n):
            if origin_list[i] > origin_list[i+1]:
                origin_list[i],origin_list[i+1] = origin_list[i+1],origin_list[i]
        n += 1
    print(origin_list)


origin_list = [2, 5, 12, 1, 0, 8]
sort_to_max(origin_list)