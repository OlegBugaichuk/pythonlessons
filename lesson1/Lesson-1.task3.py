import math
__author__ = 'Бугайчук Олег'

print('Решение квадратного трехчлена ax* + bx + x = 0')
a = int(input('Введите значение а: '))
b = int(input('Введите значение b: '))
c = int(input('Введите значение c: '))


d = b**2 - (4*a*c)

if d<0:
    print('Решений нет')
elif d == 0:
    x = -b/(2*a)
    print('x = ', x)
else:
    x1 = (-b-math.sqrt(d))/a
    x2 = (-b+math.sqrt(d))/a


    print('x1 = ', x1)
    print('x2 = ', x2)