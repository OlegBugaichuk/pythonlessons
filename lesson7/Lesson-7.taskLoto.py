__autor__ = "Бугайчук Олег"

"""
== Лото ==
Правила игры в лото.
Игра ведется с помощью специальных карточек, на которых отмечены числа, 
и фишек (бочонков) с цифрами.
Количество бочонков — 90 штук (с цифрами от 1 до 90).
Каждая карточка содержит 3 строки по 9 клеток. В каждой строке по 5 случайных цифр, 
расположенных по возрастанию. Все цифры в карточке уникальны. Пример карточки:
--------------------------
    9 43 62          74 90
 2    27    75 78    82
   41 56 63     76      86 
--------------------------
В игре 2 игрока: пользователь и компьютер. Каждому в начале выдается 
случайная карточка. 
Каждый ход выбирается один случайный бочонок и выводится на экран.
Также выводятся карточка игрока и карточка компьютера.
Пользователю предлагается зачеркнуть цифру на карточке или продолжить.
Если игрок выбрал "зачеркнуть":
	Если цифра есть на карточке - она зачеркивается и игра продолжается.
	Если цифры на карточке нет - игрок проигрывает и игра завершается.
Если игрок выбрал "продолжить":
	Если цифра есть на карточке - игрок проигрывает и игра завершается.
	Если цифры на карточке нет - игра продолжается.
	
Побеждает тот, кто первый закроет все числа на своей карточке.
Пример одного хода:
Новый бочонок: 70 (осталось 76)
------ Ваша карточка -----
 6  7          49    57 58
   14 26     -    78    85
23 33    38    48    71   
--------------------------
-- Карточка компьютера ---
 7 11     - 14    87      
      16 49    55 77    88    
   15 20     -       76  -
--------------------------
Зачеркнуть цифру? (y/n)
Подсказка: каждый следующий случайный бочонок из мешка удобно получать 
с помощью функции-генератора.
Подсказка: для работы с псевдослучайными числами удобно использовать 
модуль random: http://docs.python.org/3/library/random.html
"""

from Player import Player
import sys
import random


def game():
    i = 1

    player = Player("Player")
    comp = Player("AI")

    player.randomCard()
    comp.randomCard()

    while i <= 90:

        playerEmpty = player.isEmptyCard()

        compEmpty = comp.isEmptyCard()

        number = random.randint(1, 90)

        if playerEmpty and compEmpty == True:
            print("-----НИЧЬЯ!!!!-----")
            break
        elif playerEmpty and compEmpty == False:
            print("----ВЫ ВЫИГРАЛИ!!!----")
            break
        elif playerEmpty == False and compEmpty:
            print("----ВЫ ПРОИГРАЛИ!!!----")
            break
        else:

            print("----------Новый бочонок: {} (осталось {})----------".format(number, 90 - i))
            print("------------------Ваша карточка--------------------")
            player.getCard()
            print(" ")
            print("------------------Карточка компа-------------------")
            comp.getCard()

            rezult = input("Зачеркнуть число? y/n: ")

            if rezult == "y":
                if comp.findNumber(number):
                    comp.updateCard(number)

                if player.findNumber(number):
                    player.updateCard(number)
                else:
                    print("Вы проиграли, такого числа нет на вашей карточке!")
                    break

            elif rezult == "n":
                if comp.findNumber(number):
                    comp.updateCard(number)

                if player.findNumber(number):
                    print("Вы проиграли, у Вас есть такое число")
                    break
            else:
                break
        if i == 90:
            count_player_numbers_in_card = player.countNumbersInCard()
            count_comp_numbers_in_card = comp.countNumbersInCard()

            if count_player_numbers_in_card < count_comp_numbers_in_card:
                print("----------ВЫ ВЫИГРАЛИ------------")
                print(" ")
                print("У компа осталось {} чисел".format(count_comp_numbers_in_card))

            elif count_player_numbers_in_card > count_comp_numbers_in_card:
                print("----------ВЫ ПРОИГРАЛИ------------")
                print(" ")
                print("У Вас осталось {} чисел".format(count_player_numbers_in_card))

            else:
                print(" ")
                print("-------------Ничья------------")
                print("{} - {}".format(count_player_numbers_in_card, count_comp_numbers_in_card))

            break

        i += 1


def startGame():
    print("----------Добро пожаловать!!!----------")
    start = input("Начать игру? д/н :")
    if start == "д":
        game()
    else:
        sys.exit()


if __name__ == "__main__":
    startGame()
