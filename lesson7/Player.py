__autor__ = "Бугайчук Олег"
import random

class Player:



    def __init__(self, name, card = [[],[],[]]):
        self.__name = name
        self.__card = card


    def randomCard(self):
        '''
        Method create random card for player
        '''
        card = [[], [], []]
        strings = []
        strings = random.sample(range(1, 91), 15)

        i = 0

        for j in range(3):
            for k in range(5):
                card[j].append(strings[i])
                i += 1
                k += 1
            card[j] = list(card[j]) + list(' ' *4)
            random.shuffle(card[j])
            j += 1

        self.__card = card





    def findNumber(self, number):
        '''
        method find number in card and return true or false
        '''

        in_list = False
        for card in self.__card:
            if number in card:
                in_list = True
                break

        return in_list

    def updateCard(self, number):
        '''
        Method for update card if card have number
        '''
        index1 = 0
        index2 = 0
        for i in range(3):
            try:
                index2 = self.__card[i].index(number)
                index1 = i
                break
            except ValueError:
                pass
            i += 1

        self.__card[index1][index2] = "-"
        return self.__card


    def isEmptyCard(self):
        '''
        Method to check card for numbers
        '''
        rezult = True
        for i in range(3):
            for elem in self.__card[i]:
                if elem != " " and elem != "-":
                    rezult = False

        return rezult

    def countNumbersInCard(self):
        '''
        Method count numbers in card
        '''
        rezult = 0
        for i in range(3):
            for elem in self.__card[i]:
                if elem != " " and elem != "-":
                    rezult += 1

        return rezult

    def getCard(self):
        '''
        Method print card
        '''
        print('{0[0]} {0[1]} {0[2]} {0[3]} {0[4]} {0[5]} {0[6]} {0[7]} {0[8]}'.format(self.__card[0]))
        print('{0[0]} {0[1]} {0[2]} {0[3]} {0[4]} {0[5]} {0[6]} {0[7]} {0[8]}'.format(self.__card[1]))
        print('{0[0]} {0[1]} {0[2]} {0[3]} {0[4]} {0[5]} {0[6]} {0[7]} {0[8]}'.format(self.__card[2]))
        print('{:-^25}'.format( '-'))




    def getName(self):
        '''
        Method return name player
        '''

        return self.__name
