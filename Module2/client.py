# Скрипт клиента
from Config import Client
from socket import *
import json
from sys import argv
import sendData
import logging

logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s\
    [%(asctime)s] %(message)s', level = logging.DEBUG, filename = u'client.log')

client = Client('localhost')


if len(argv) > 1:
    client.setAddress(argv[1])
    if len(argv) > 2:
        client.setPort(int(argv[2]))



def connect():
    print("Выберите ---    auth / message / exit и введите")
    request = input()
    s = socket(AF_INET, SOCK_STREAM)  # Создать сокет TCP
        
    if request == "auth":
        
        login = input("login: ")
        password = input("password: ")
        auth_data = sendData.auth(login,password)
        
        s.connect((client.getAddress(), client.getPort()))
        logging.info("Соединение с сервером")
          # Соединиться с сервером
        s.send(auth_data.encode('ascii'))

        responce = s.recv(1024)  # Принять не более 1024 байтов данных
        responce = responce.decode('ascii')
        responce = json.loads(responce)

        if responce['code'] == '200':
            print(responce['message'])
            logging.info("Успешная авторизация")
        else:
            print(responce['message']) 
            logging.critical("Авторизация не прошла")
        logging.info("Соединение с сервером разорвано")
        s.close()

    elif request == "message":
        
        me = input("Кто вы? ")
        user = input("Кому сообщение? ")
        message = input("Введите сообщение: ")
        msg_data = sendData.msg(message, me, user)

        s.connect((client.getAddress(), client.getPort()))
        logging.info("Соединение с сервером")
        s.send(msg_data.encode('ascii'))

        responce = s.recv(1024)  # Принять не более 1024 байтов данных
        responce = responce.decode('ascii')
        responce = json.loads(responce)

        if responce['code'] == '200':
            print(responce['message'])
            logging.info("Сообщение отправлено")
        else:
            print(responce['message']) 
            logging.critical("Сообщение не удалось отправить")
        logging.info("Соединение с сервером разорвано")
        s.close()
    elif request == "exit":
        
        exit(0)


if __name__ == "__main__":
    
    while True:
        connect()