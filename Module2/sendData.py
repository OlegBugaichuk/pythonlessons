"""
для отправки json 
"""


def auth(login, password):
    '''
    generic json for send auth
    '''
    auth_data = r'{"action": "authenticate","user":{"account_name": "%s","pasword" : "%s" }}' % (login, password)
    return auth_data

def msg(message, me, user):
    '''
    generic json for send message
    '''
    msg_data = r'{"action": "msg", "time" : "-", "to": "%s", "from": "%s", "encoding": "ascii", "message": "%s"}' % (user, me, message)
    return msg_data

def responce(code, message):
    '''
    generic json for send responce
    '''
    responce_data = r'{"action": "responce", "time" : "-", "code": "%s", "message": "%s"}' % (code, message)
    return responce_data
