# Скрипт сервера 
from Config import Server
from socket import *
from sys import argv
import sendData
import json
import logging
from threading import Thread

server = Server()

logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s\
    [%(asctime)s] %(message)s', level = logging.DEBUG, filename = u'server.log')

if "-p" in argv:
    index = argv.index("-p")
    server.setPort(int(argv[index + 1]))
if "-a" in argv:
    index = argv.index("-a")
    server.setAddress(argv[index + 1])  

def connect_client(client, addr):
      # Принять запрос на соединение
    logging.info("Получен запрос на соединение от %s", str(addr))
    try:
        data = client.recv(1024) 
        data = data.decode('ascii')
        data = json.loads(data)
        if data['action'] == 'authenticate':

            responceData =  sendData.responce("200", "Connected")
            client.send(responceData.encode('ascii'))
            logging.info("Пользователь %s  присоединился", data['user']['account_name'])
            
        elif data['action'] == 'msg':
            
            responceData =  sendData.responce("200", "Message sent")
            client.send(responceData.encode('ascii'))
            print("Пользователь {} пишет {} : {}".format(data['from'], data['to'], data['message']))
        else:
            responceData =  sendData.responce("404", "ooooopssss")
            client.send(responceData.encode('ascii'))
            print("Не правильный запрос")

            logging.critical("Не правильный запрос")
        
    except:
        logging.critical("Ошибка соединения")
        print("Ошибка соединения")
    
    #logging.info("Соединение с %s прервано", str(addr))
    

def run_server():
    s = socket(AF_INET, SOCK_STREAM)  # Создает сокет TCP
    s.bind((server.getAddress(), server.getPort()))  # Присваивает порт 7777
    print("----------Сервер запущен--------- ")
    s.listen(5)

    while True:
        client, addr = s.accept()

        t = Thread(target=connect_client, args=(client, addr))
        t.start()
    
if __name__ == "__main__":
    run_server()




