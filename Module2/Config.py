class Server:

    """
    конфиг
    """
    def __init__(self, addr = '', port = 7777):
        self.__addr = addr
        self.__port = port

    def setAddress(self, addr):
        self.__addr = addr

    def setPort(self, port):
        self.__port = port

    def getPort(self):
        return self.__port
    
    def getAddress(self):
        return self.__addr



class Client:

    """
    конфиг
    """
    def __init__(self, addr = 'localhost', port = 7777):
        self.__addr = addr
        self.__port = port

    def setAddress(self, addr):
        self.__addr = addr

    def setPort(self, port):
        self.__port = port

    def getPort(self):
        return self.__port
    
    def getAddress(self):
        return self.__addr