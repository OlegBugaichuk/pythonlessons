import pytest
import sys

sys.path.append('../')
import sendData


def test_sendData_auth():
    assert(sendData.auth('Oleg', '123') == r'{"action": "authenticate","user":{"account_name": "Oleg","pasword" : "123" }}')

def test_sendData_msg():
    assert(sendData.msg('test', 'Oleg', 'Andrey') == r'{"action": "msg", "time" : "-", "to": "Andrey", "from": "Oleg", "encoding": "ascii", "message": "test"}')